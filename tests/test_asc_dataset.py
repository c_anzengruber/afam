import unittest
import shutil
import os

import afam


class TestASC_Dataset(unittest.TestCase):
    """
    Providing all unit tests for the class ASC_Dataset
    """

    def test_to_mat(self):
        asc_dataset = afam.read_asc('./tests/data/mono250.asc')
        asc_dataset.to_mat('./tests/data/test_mono250.mat')
        os.remove('./tests/data/test_mono250.mat')

    def test_to_ascDS(self):
        asc_dataset = afam.read_asc('./tests/data/convert.asc')
        asc_dataset.to_ascDS('convert')
        shutil.rmtree('convert')
