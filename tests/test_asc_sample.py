import unittest

from afam.asc_sample import ASC_SAMPLE


class TestASC_SAMPLE(unittest.TestCase):
    """
    Providing all unit tests for the class ASC_MSG
    """

    def test_to_dict(self):
        sample_dict = {'t': 1}
        sample_asc = ASC_SAMPLE(t=1)
        self.assertEqual(sample_asc.to_dict(), sample_dict)
