import unittest
import os

import afam


class TestASC_SAMPLE(unittest.TestCase):
    """
    Providing all unit tests for the afam functions
    """

    def test_read_asc(self):
        for f in os.listdir("./tests/data/"):
            if os.path.splitext(f)[1] == '.asc':
                asc_dataset = afam.read_asc(f"./tests/data/{f}", search=["FIXATION_POS"])
                assert isinstance(asc_dataset, afam.ASC_Dataset)
