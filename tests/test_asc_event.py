import unittest

from afam.asc_event import ASC_EVENT


class TestASC_EVENT(unittest.TestCase):
    """
    Providing all unit tests for the class ASC_MSG
    """

    def test_to_dict(self):
        event_dict = {}
        event_asc = ASC_EVENT()
        self.assertEqual(event_asc.to_dict(), event_dict)
