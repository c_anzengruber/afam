:py:mod:`afam.asc_dataclasses`
==============================

.. py:module:: afam.asc_dataclasses

.. autoapi-nested-parse::

   This module provides all supported ASCII event classes.



Module Contents
---------------

Classes
~~~~~~~

.. autoapisummary::

   afam.asc_dataclasses.ASC_EVENT
   afam.asc_dataclasses.ASC_BUTTON
   afam.asc_dataclasses.ASC_INPUT
   afam.asc_dataclasses.ASC_SBLINK
   afam.asc_dataclasses.ASC_SSACC
   afam.asc_dataclasses.ASC_SFIX
   afam.asc_dataclasses.ASC_EBLINK
   afam.asc_dataclasses.ASC_ESACC
   afam.asc_dataclasses.ASC_EFIX
   afam.asc_dataclasses.ASC_MSG




.. py:class:: ASC_EVENT

   A dataclass representing the basic framework of every ASCII event.

   Attribute
   ---------
   `id` : int
       trial id of the event

   .. py:attribute:: id
      :annotation: :int

      

   .. py:method:: to_dict()

      Convert the ASC_EVENT class to a dictionary.

      :returns: **`self.__dict__`** -- Dictionary representing all attributes of the ASCII event
      :rtype: dict

      .. rubric:: Example

      >>> event = ASC_EVENT(id=1)
      >>> event.to_dict()
      {'id': 1}



.. py:class:: ASC_BUTTON

   Bases: :py:obj:`ASC_EVENT`

   A dataclass used to store the data from a "BUTTON" line.
   This line contains a button press or release event.

   .. attribute:: `id`

      trial id of the event

      :type: int

   .. attribute:: `t`

      time of button press

      :type: int

   .. attribute:: `b`

      button number (1-8)

      :type: int

   .. attribute:: `s`

      button change (1=pressed, 0=released)

      :type: int

   .. py:attribute:: t
      :annotation: :int

      

   .. py:attribute:: b
      :annotation: :int

      

   .. py:attribute:: s
      :annotation: :int

      


.. py:class:: ASC_INPUT

   Bases: :py:obj:`ASC_EVENT`

   A dataclass used to store the data from a "INPUT" line.
   This line contains an input event.

   .. attribute:: `id`

      trial id of the event

      :type: int

   .. attribute:: `t`

      time of input

      :type: int

   .. attribute:: `p`

      port number

      :type: int

   .. py:attribute:: t
      :annotation: :int

      

   .. py:attribute:: p
      :annotation: :int

      


.. py:class:: ASC_SBLINK

   Bases: :py:obj:`ASC_EVENT`

   A dataclass used to store the data from a "SBLINK" line.
   This line contains the start of a blink event.

   .. attribute:: `id`

      trial id of the event

      :type: int

   .. attribute:: `eye`

      eye (L-R)

      :type: str

   .. attribute:: `st`

      start time

      :type: int

   .. py:attribute:: eye
      :annotation: :str

      

   .. py:attribute:: st
      :annotation: :int

      


.. py:class:: ASC_SSACC

   Bases: :py:obj:`ASC_EVENT`

   A dataclass used to store the data from a "SSACC" line.
   This line contains the start of a saccade event.

   .. attribute:: `id`

      trial id of the event

      :type: int

   .. attribute:: `eye`

      eye (L-R)

      :type: str

   .. attribute:: `st`

      start time

      :type: int

   .. py:attribute:: eye
      :annotation: :str

      

   .. py:attribute:: st
      :annotation: :int

      


.. py:class:: ASC_SFIX

   Bases: :py:obj:`ASC_EVENT`

   A dataclass used to store the data from a "SFIX" line.
   This line contains the start of a fixation event.

   .. attribute:: `id`

      trial id of the event

      :type: int

   .. attribute:: `eye`

      eye (L-R)

      :type: str

   .. attribute:: `st`

      start time

      :type: int

   .. py:attribute:: eye
      :annotation: :str

      

   .. py:attribute:: st
      :annotation: :int

      


.. py:class:: ASC_EBLINK

   Bases: :py:obj:`ASC_EVENT`

   A dataclass used to store the data from a "EBLINK" line.
   This line contains start and end time (actually the time of the last sample fully within the
   blink), plus summary data of a blink event.

   .. attribute:: `id`

      trial id of the event

      :type: int

   .. attribute:: `eye`

      eye (L-R)

      :type: str

   .. attribute:: `st`

      start time

      :type: int

   .. attribute:: `et`

      end time

      :type: int

   .. attribute:: `d`

      duration

      :type: int

   .. py:attribute:: eye
      :annotation: :str

      

   .. py:attribute:: st
      :annotation: :int

      

   .. py:attribute:: et
      :annotation: :int

      

   .. py:attribute:: d
      :annotation: :int

      


.. py:class:: ASC_ESACC

   Bases: :py:obj:`ASC_EVENT`

   A dataclass used to store the data from a "ESACC" line.
   This line contains start and end time (actually the time of the last sample fully within the
   saccade), plus summary data of a saccade event.

   .. attribute:: `id`

      trial id of the event

      :type: int

   .. attribute:: `eye`

      eye (L-R)

      :type: str

   .. attribute:: `st`

      start time

      :type: int

   .. attribute:: `et`

      end time

      :type: int

   .. attribute:: `d`

      duration

      :type: int

   .. attribute:: `sx`

      start X position

      :type: float

   .. attribute:: `sy`

      start Y position

      :type: float

   .. attribute:: `ex`

      end X position

      :type: float

   .. attribute:: `ey`

      end Y position

      :type: float

   .. attribute:: `ampl`

      amplitude in degrees

      :type: float

   .. attribute:: `pvel`

      peak velocity, degr/sec

      :type: float

   .. attribute:: `resx`

      resolution (if events_have_resolution==1)

      :type: float

   .. attribute:: `resy`

      resolution (if events_have_resolution==1)

      :type: float

   .. py:attribute:: eye
      :annotation: :str

      

   .. py:attribute:: st
      :annotation: :int

      

   .. py:attribute:: et
      :annotation: :int

      

   .. py:attribute:: d
      :annotation: :int

      

   .. py:attribute:: sx
      :annotation: :float

      

   .. py:attribute:: sy
      :annotation: :float

      

   .. py:attribute:: ex
      :annotation: :float

      

   .. py:attribute:: ey
      :annotation: :float

      

   .. py:attribute:: ampl
      :annotation: :float

      

   .. py:attribute:: pvel
      :annotation: :float

      

   .. py:attribute:: resx
      :annotation: :float

      

   .. py:attribute:: resy
      :annotation: :float

      


.. py:class:: ASC_EFIX

   Bases: :py:obj:`ASC_EVENT`

   A dataclass used to store the data from a "EFIX" line.
   This line contains start and end time (actually the time of the last sample fully within the
   fixation), plus summary data of a fixation event.

   .. attribute:: `id`

      trial id of the event

      :type: int

   .. attribute:: `eye`

      eye (L-R)

      :type: str

   .. attribute:: `st`

      start time

      :type: int

   .. attribute:: `et`

      end time

      :type: int

   .. attribute:: `d`

      duration

      :type: int

   .. attribute:: `x`

      X position

      :type: float

   .. attribute:: `y`

      Y position

      :type: float

   .. attribute:: `resx`

      resolution (if events_have_resolution==1)

      :type: float

   .. attribute:: `resy`

      resolution (if events_have_resolution==1)

      :type: float

   .. py:attribute:: eye
      :annotation: :str

      

   .. py:attribute:: st
      :annotation: :int

      

   .. py:attribute:: et
      :annotation: :int

      

   .. py:attribute:: d
      :annotation: :int

      

   .. py:attribute:: x
      :annotation: :float

      

   .. py:attribute:: y
      :annotation: :float

      

   .. py:attribute:: resx
      :annotation: :float

      

   .. py:attribute:: resy
      :annotation: :float

      


.. py:class:: ASC_MSG

   Bases: :py:obj:`ASC_EVENT`

   A dataclass used to store the data from a "MSG" line.
   This line contains token and start time, plus some summary data.

   .. attribute:: `id`

      trial id of the event

      :type: int

   .. attribute:: `t`

      token (TRIALID, SYNCTIME, TRIAL_RESULT, INFO, etc.)

      :type: str

   .. attribute:: `st`

      start time

      :type: int

   .. attribute:: `data`

      information

      :type: str

   .. py:attribute:: t
      :annotation: :str

      

   .. py:attribute:: st
      :annotation: :int

      

   .. py:attribute:: data
      :annotation: :str

      


